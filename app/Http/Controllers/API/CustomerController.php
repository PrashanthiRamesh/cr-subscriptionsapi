<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @SWG\Get(
 *   path="/customer/{id}",
 *   summary="List customer specific to ID",
 *   operationId="listOne",
 *   @SWG\Parameter(
 *     name="customer_id",
 *     in="path",
 *     description="Target customer.",
 *     required=true,
 *     type="integer"
 *   ),
 *
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=406, description="not acceptable"),
 *   @SWG\Response(response=500, description="internal server error")
 * )
 *
 */

class CustomerController extends Controller
{

    public function create(){

    }

    public function listAll(){
        return ":)";
    }

    public function listOne($customer_id){

    }

    public function update(){

    }

    public function remove(){

    }
}
