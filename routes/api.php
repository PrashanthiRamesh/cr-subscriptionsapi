<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){

    //test route
    Route::get('details', 'API\UserController@details');

    /**
     * Customer Routes
     */

    Route::post('customer/create', 'API\CustomerController@create');
    Route::get('customers', 'API\CustomerController@listAll');
    Route::get('customer/{id}', 'API\CustomerController@listOne');
    Route::put('customer/{id}', 'API\CustomerController@update');
    Route::delete('customer/{id}', 'API\CustomerController@remove');

    /**
     * Plan Routes
     */

    Route::post('plan/create', 'API\PlanController@create');
    Route::get('plans', 'API\PlanController@listAll');
    Route::get('plan/{id}', 'API\PlanController@listOne');
    Route::put('plan/{id}', 'API\PlanController@update');
    Route::delete('plan/{id}', 'API\PlanController@remove');

    /**
     * Subscription Routes
     */

    Route::post('subscription/create', 'API\SubscriptionController@create');
    Route::get('subscriptions', 'API\SubscriptionController@listAll');
    Route::get('subscription/{id}', 'API\SubscriptionController@listOne');
    Route::put('subscription/{id}', 'API\SubscriptionController@update');
    Route::delete('subscription/{id}', 'API\SubscriptionController@remove');
});