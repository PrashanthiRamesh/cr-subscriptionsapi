<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
    }
}
class UsersTableSeeder extends Seeder {

    public function run()
    {
        //delete users table records
        DB::table('users')->delete();
        //insert some dummy records
        DB::table('users')->insert(array(
            array('name'=>'arya','email'=>'arya@got.com','password'=>\Illuminate\Support\Facades\Hash::make('qwerty')),

        ));
    }

}